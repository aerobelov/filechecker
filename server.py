import socket
import sys

if int(sys.argv[1]) in range(1, 65535):
    sock = socket.socket()
    sock.bind(('', int(sys.argv[1])))
    sock.listen(1024)
    while True:
        conn, addr = sock.accept()
        data = conn.recv(5024)
        if not data:
            continue
        else: 
            print(data.decode('utf-8'))
else:
    print("Invalid port in cli parameter")

conn.close()