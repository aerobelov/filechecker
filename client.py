import re
import os
import time
import sys
import socket
from datetime import datetime

#Send to socket
def send(self, info, HOST, PORT):
    with socket.socket() as sock:
        sock.connect((HOST, PORT))
        sock.send(info.encode('utf-8'))
        sock.close()

#filechecker class
class FileChecker(object):
    dir = ''
    current = set()
    file_dict = {}
    host = ''
    port = 0

    def __init__(self, list):
        self.dir = list[1]
        self. host = list[2]
        self.port = int(list[3])

    #Walk over directory, save timestamp, size to dir, keys to set
    def get_dir(self):
        self.current = ()
        self.file_dict = {}
        for r, d, f in os.walk(self.dir):
            for file in f:
                fpath = os.path.join(r, file)
                self.file_dict[fpath] = (os.path.getsize(fpath), os.path.getmtime(fpath))
        self.current = set(self.file_dict.keys())

    #Check modified files generator
    def check_modified(self, prev_dict, fwd_dict):
        for key in  prev_dict:
            if key in fwd_dict.keys():
                if prev_dict[key][1] != fwd_dict[key][1]:
                    yield (key, fwd_dict[key][1])

    #Compare function
    def check(self):
        #Previous files set
        prev = self.current
        #Previous files size and lastmod time dict
        prev_dict = self.file_dict
        self.get_dir()
        #Check if we lost files
        lost = prev.difference(self.current)
        #Check if we have added files
        added = self.current.difference(prev)
        #Message - information to send
        message = ''
        if len(lost) > 0:
            for item in lost:
                message += (datetime.now().strftime('%Y-%m-%d %H:%M') + ' DELETED: ' + item  +\
                     " Size: " + str(prev_dict[item][0]) + " Bytes\n")
        if len(added) > 0:
            for item in added:
                message += (datetime.now().strftime('%Y-%m-%d %H:%M') + ' ADDED: ' + item + \
                    " Size: " + str(self.file_dict[item][0]) + " Bytes\n")
        #Check modified files generator
        gen = self.check_modified(prev_dict, self.file_dict)
        for i in gen:
            message += (datetime.now().strftime('%Y-%m-%d %H:%M') + " MODIFIED: " + i[0] + \
                " on " + datetime.utcfromtimestamp(i[1]).strftime('%Y-%m-%d %H:%M') + "UTC\n")
        send(self, message, self.host, self.port)

#Check if cli parameters are valid        
def check_parms():
    return True if (\
        sys.argv[1] and\
        re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', sys.argv[2]) and\
        re.match('\d{1,5}', sys.argv[3]) and (int(sys.argv[3]) in range(1, 65535))\
        )\
    else False

#Main
if check_parms():
    a = FileChecker(sys.argv)
    while True:
        a.check()
        time.sleep(2)
else:
    print("Invalid params, check {dir}[ex. /home] {host}[ex. 127.0.0.1] {port}[ex. 5000]")
    